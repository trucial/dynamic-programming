
/*
Given an integer n, count the total number of digit 1 appearing in all
non-negative integers less than or equal to n.
*/

class Solution {
public:
    int countDigitOne(int n)
    {
        if (n == 0) {
            return 0;
        }

        if (n < 10) {
            return 1;
        }

        if (dp.empty()) {
            count(n);
        }

        int pow = pow10(std::log10(n));

        int ones = n / pow * dp[pow - 1];
        ones += n / pow > 1 ? pow : 0;
        ones += n / pow == 1 ? (n % pow) + 1 : 0;
        ones += countDigitOne(n % pow);

        return ones;
    }

private:
    std::unordered_map<long int, long int> dp;

    void count(int n)
    {
        dp[9] = 1;

        for (long int e = 9; e < n; e = (e * 10) + 9) {
            dp[e * 10 + 9] = 10 * dp[e] + (e + 1);
        }
    }

    int pow10(int exp)
    {
        int pow = 1;

        for (int i = 0; i < exp; ++i) {
            pow *= 10;
        }

        return pow;
    }
};
