
/*
There is an undirected connected tree with n nodes labeled from 0 to n - 1 and
n - 1 edges.

You are given the integer n and the array edges where edges[i] = [ai, bi]
indicates that there is an edge between nodes ai and bi in the tree.

Return an array answer of length n where answer[i] is the sum of the distances
between the ith node in the tree and all other nodes.
*/

class Solution {
public:
    std::vector<int> sumOfDistancesInTree(int n, std::vector<std::vector<int>>& edges)
    {
        std::vector<std::vector<int>> adjacency(n);

        for (const auto& edge : edges) {
            adjacency.at(edge.front()).push_back(edge.back());
            adjacency.at(edge.back()).push_back(edge.front());
        }

        std::vector<int> count(n, 1);
        std::vector<int> sum(n, 0);

        // Calculate count and sum of distances for every subtree rooted at node
        std::function<void(int, int)> dfs = [&adjacency, &count, &sum, &dfs] (int node, int parent) mutable
        {
            for (const auto child : adjacency.at(node)) {
                if (child != parent) {
                    dfs(child, node);
                    count.at(node) += count.at(child);
                    sum.at(node) += sum.at(child) + count.at(child);
                }
            }
        };

        dfs(0, std::numeric_limits<int>::min());

        // Calculate sum of distances from every node
        std::function<void(int, int)> dfs2 = [n, &adjacency, &sum, &count, &dfs2] (int node, int parent) mutable
        {
            for (const auto child : adjacency.at(node)) {
                if (child != parent) {
                    sum.at(child) = sum.at(node) + n - 2 * count.at(child);
                    dfs2(child, node);
                }
            }
        };

        dfs2(0, std::numeric_limits<int>::min());

        return sum;
    }
};
