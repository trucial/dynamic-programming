
/*
You are given an integer array prices where prices[i] is the price of a given
stock on the ith day, and an integer k.

Find the maximum profit you can achieve. You may complete at most k
transactions: i.e. you may buy at most k times and sell at most k times.

Note: You may not engage in multiple transactions simultaneously (i.e., you
must sell the stock before you buy again).
*/

class Solution {
public:
    int maxProfit(int k, std::vector<int>& prices)
    {
        if (prices.size() < 2) {
            return 0;
        }

        if (2 * k > prices.size()) {
            std::adjacent_difference(prices.begin(), prices.end(), prices.begin(), [] (int x, int y) { return std::max(x - y, 0); });
            return std::reduce(std::next(prices.begin()), prices.end());
        }

        std::vector<int> buy(k + 1, std::numeric_limits<int>::min());
        std::vector<int> sell(k + 1, 0);

        for (const auto& price : prices) {
            for (int i = 1; i < k + 1; ++i) {
                buy.at(i) = std::max(buy.at(i), sell.at(i - 1) - price);
                sell.at(i) = std::max(sell.at(i), buy.at(i) + price);
            }
        }

        return sell.at(k);
    }
};
