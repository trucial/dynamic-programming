
/*
We can scramble a string s to get a string t using the following algorithm:

1. If the length of the string is 1, stop.
2. If the length of the string is > 1, do the following:

Split the string into two non-empty substrings at a random index, i.e., if the
string is s, divide it to x and y where s = x + y.

Randomly decide to swap the two substrings or to keep them in the same order.
i.e., after this step, s may become s = x + y or s = y + x.

Apply step 1 recursively on each of the two substrings x and y.

Given two strings s1 and s2 of the same length, return true if s2 is a
scrambled string of s1, otherwise, return false.
*/

class Solution {
public:
    bool isScramble(std::string s1, std::string s2)
    {
        if (s1 == s2) {
            return dp[s1 + '-' + s2] = true;
        }

        if (s1.size() == 1) {
            return false;
        }

        if (dp.contains(s1 + '-' + s2)) {
            return dp.at(s1 + '-' + s2);
        }

        bool scrambled{};

        for (std::size_t i = 1, n = s1.size(); i < n; ++i) {
            scrambled = isScramble(s1.substr(0, i), s2.substr(0, i)) && isScramble(s1.substr(i), s2.substr(i)) ||
                        isScramble(s1.substr(0, i), s2.substr(n - i)) && isScramble(s1.substr(i), s2.substr(0, n - i));
            if (scrambled) {
                return dp[s1 + '-' + s2] = true;
            }
        }

        dp[s1 + '-' + s2] = false;

        return scrambled;
    }

private:
   std::unordered_map<std::string, bool> dp;
};
