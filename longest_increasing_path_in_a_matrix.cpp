
/*
Given an m x n integers matrix, return the length of the longest increasing
path in matrix.

From each cell, you can either move in four directions: left, right, up, or
down. You may not move diagonally or move outside the boundary (i.e.,
wrap-around is not allowed).
*/

class Solution {
public:
    int longestIncreasingPath(std::vector<std::vector<int>>& matrix)
    {
        rows_ = matrix.size();
        columns_ = matrix.front().size();
        matrix_ = std::move(matrix);

        dp_.resize(rows_, std::vector<int>(columns_));

        for (int i = 0; i < rows_; ++i) {
            for (int j = 0; j < columns_; ++j) {
                dfs_(i, j, -1);
            }
        }

        int ans = std::numeric_limits<int>::min();
        for (const auto& column : dp_) {
            ans = std::max(ans, *std::max_element(column.begin(), column.end()));
        }
        return ans;
    }

private:
    int rows_;
    int columns_;
    std::vector<std::vector<int>> matrix_;
    std::vector<std::vector<int>> dp_;

    int dfs_(int m, int n, int previous)
    {
        if (m < 0 || m == rows_ || n < 0 || n == columns_ || previous >= matrix_.at(m).at(n)) {
            return 0;
        }

        if (dp_.at(m).at(n) != 0) {
            return dp_.at(m).at(n);
        }

        int max = 1;
        max = std::max(max, 1 + dfs_(m - 1, n, matrix_.at(m).at(n)));
        max = std::max(max, 1 + dfs_(m + 1, n, matrix_.at(m).at(n)));
        max = std::max(max, 1 + dfs_(m, n - 1, matrix_.at(m).at(n)));
        max = std::max(max, 1 + dfs_(m, n + 1, matrix_.at(m).at(n)));
        return dp_.at(m).at(n) = max;
    }
};
