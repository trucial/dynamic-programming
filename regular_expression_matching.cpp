
/*
Given an input string s and a pattern p, implement regular expression matching
with support for '.' and '*' where:

'.' Matches any single character.
'*' Matches zero or more of the preceding element.

The matching should cover the entire input string (not partial).
*/

class Solution {
public:
    bool isMatch(std::string s, std::string p)
    {
        const int m = s.size();
        const int n = p.size();

        std::vector<std::vector<bool>> dp(m + 1, std::vector<bool>(n + 1, false));

        dp.front().front() = true;

        for (int j = 1; j < n + 1; ++j) {
            dp.front().at(j) = p.at(j - 1) == '*' && j > 1 && dp.front().at(j - 2);
        }

        for (int i = 1; i < m + 1; ++i) {
            for (int j = 1; j < n + 1; ++j) {
                if (p.at(j - 1) == '*') {
                    dp.at(i).at(j) = dp.at(i).at(j - 2) || (s.at(i - 1) == p.at(j - 2) || p.at(j - 2) == '.') && dp.at(i -1).at(j);
                } else {
                    dp.at(i).at(j) = dp.at(i - 1).at(j - 1) && (s.at(i - 1) == p.at(j - 1) || p.at(j - 1) == '.');
                }
            }
        }

        return dp.back().back();
    }
};
