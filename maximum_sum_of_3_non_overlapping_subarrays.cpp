
/*
Given an integer array nums and an integer k, find three non-overlapping
subarrays of length k with maximum sum and return them.

Return the result as a list of indices representing the starting position of
each interval (0-indexed). If there are multiple answers, return the
lexicographically smallest one.
*/

class Solution {
public:
    std::vector<int> maxSumOfThreeSubarrays(std::vector<int>& nums, int k)
    {
        std::vector<int> best_1 = {0};
        std::vector<int> best_2 = {0, k};
        std::vector<int> best_3 = {0, k, k * 2};

        auto current_1 = std::reduce(nums.begin(), nums.begin() + k);
        auto current_2 = std::reduce(nums.begin() + k, nums.begin() + k * 2);
        auto current_3 = std::reduce(nums.begin() + k * 2, nums.begin() + k * 3);

        auto max_1 = std::reduce(nums.begin(), nums.begin() + k);
        auto max_2 = std::reduce(nums.begin(), nums.begin() + k * 2);
        auto max_3 = std::reduce(nums.begin(), nums.begin() + k * 3);

        for (int i = 1; i < nums.size() - 3 * k + 1; ++i) {
            current_1 += nums.at(k + i - 1) - nums.at(i - 1);
            current_2 += nums.at(2 * k + i - 1) - nums.at(k + i - 1);
            current_3 += nums.at(3 * k + i - 1) - nums.at(2 * k + i - 1);

            if (max_1 < current_1) {
                max_1 = current_1;
                best_1 = {i};
            }

            if (max_2 < current_2 + max_1) {
                max_2 = current_2 + max_1;
                best_2 = {best_1.front(), i + k};
            }

            if (max_3 < current_3 + max_2) {
                max_3 = current_3 + max_2;
                best_3 = {best_2.front(), best_2.back(), i + k * 2};
            }
        }

        return best_3;
    }
};
