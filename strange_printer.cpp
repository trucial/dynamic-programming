/*
There is a strange printer with the following two special properties:

The printer can only print a sequence of the same character each time.

At each turn, the printer can print new characters starting from and ending at
any place and will cover the original existing characters.

Given a string s, return the minimum number of turns the printer needed to print it.
*/

struct hash0 {
    template <typename T1, typename T2>
    std::size_t operator() (const std::pair<T1, T2>& p) const {
        auto h1 = std::hash<T1>{}(p.first);
        auto h2 = std::hash<T2>{}(p.second);
        return h1 ^ h2;
    }
};

class Solution {
public:
    int strangePrinter(std::string s)
    {
        return solve(s, 0, s.size() - 1);
    }

private:
    std::unordered_map<std::pair<int, int>, int, hash0> dp;

    int solve(const std::string& s, int i, int j)
    {
        if (i == j) {
            return 1;
        }

        if (dp.contains({i, j})) {
            return dp[{i, j}];
        }

        auto min = std::numeric_limits<int>::max();

        for (int k = i; k < j; ++k) {
            min = std::min(min, solve(s, i, k) + solve(s, k + 1, j));
        }

        return dp[{i, j}] = s.at(i) == s.at(j) ? min - 1 : min;
    }
};
