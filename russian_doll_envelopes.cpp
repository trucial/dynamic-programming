
/*
You are given a 2D array of integers envelopes where envelopes[i] = [wi, hi]
represents the width and the height of an envelope.

One envelope can fit into another if and only if both the width and height of
one envelope are greater than the other envelope's width and height.

Return the maximum number of envelopes you can Russian doll (i.e., put one
inside the other).

Note: You cannot rotate an envelope.
*/

class Solution {
public:
    int maxEnvelopes(std::vector<std::vector<int>>& envelopes)
    {
        // Sort first by front element (width) and then by descending second element (height)
        std::sort(envelopes.begin(), envelopes.end(), [] (std::vector<int>& x, std::vector<int>& y) -> bool {
            return x.front() == y.front() ? x.back() > y.back() : x.front() < y.front();
        });

        // Remove duplicates
        auto last = std::unique(envelopes.begin(), envelopes.end());
        envelopes.erase(last, envelopes.end());

        std::vector<int> dp;
        dp.reserve(envelopes.size());

        /*
        Looping over the elements, ignoring the first element (width) as it is already ascending sorted;
        the second element (height) is in descending order, allowing us to take the smallest height for a given width
        */
        for (const auto& envelope : envelopes) {
            auto height = envelope.back();
            auto index = std::distance(dp.begin(), std::lower_bound(dp.begin(), dp.end(), height));

            if (index >= dp.size()) {
                dp.push_back(height);
            } else {
                dp.at(index) = height;
            }
        }

        return dp.size();
    }
};
