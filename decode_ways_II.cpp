
/*
A message containing letters from A-Z can be encoded into numbers using the
following mapping:

'A' -> "1" 'B' -> "2" ... 'Z' -> "26"

To decode an encoded message, all the digits must be grouped then mapped back
into letters using the reverse of the mapping above (there may be multiple
ways). For example, "11106" can be mapped into:

"AAJF" with the grouping (1 1 10 6) "KJF" with the grouping (11 10 6)

Note that the grouping (1 11 06) is invalid because "06" cannot be mapped into
'F' since "6" is different from "06".

In addition to the mapping above, an encoded message may contain the '*'
character, which can represent any digit from '1' to '9' ('0' is excluded). For
example, the encoded message "1*" may represent any of the encoded messages
"11", "12", "13", "14", "15", "16", "17", "18", or "19". Decoding "1*" is
equivalent to decoding any of the encoded messages it can represent.

Given a string s consisting of digits and '*' characters, return the number of
ways to decode it.

Since the answer may be very large, return it modulo 10^9 + 7.
*/

class Solution {
public:
    int numDecodings(std::string s)
    {
        if (s.front() == '0') {
            return 0;
        }

        if (s.size() == 1) {
            if (s.front() == '*') {
                return 9;
            } else {
                return 1;
            }
        }

        // Create and populate map
        std::array<int, 26> digit;
        std::iota(digit.begin(), digit.end(), 1);
        std::unordered_map<std::string, int> m;

        // 1..26
        std::ranges::transform(digit,
                       std::inserter(m, m.end()),
                       [] (const int i) { return std::pair(std::to_string(i), 1); });

        // *0..*6
        m["*0"] = 2;
        std::transform(digit.begin(), std::next(digit.begin(), 6),
                       std::inserter(m, m.end()),
                       [] (const int i) { return std::pair('*' + std::to_string(i), 2); });

        // *7..*9
        std::transform(std::next(digit.begin(), 6), std::next(digit.begin(), 10),
                       std::inserter(m, m.end()),
                       [] (const int i) { return std::pair('*' + std::to_string(i), 1); });

        m["*"] = 9;
        m["1*"] = 9;
        m["2*"] = 6;
        m["**"] = 15;

        std::vector<long int> dp(s.size());

        // Last two characters
        *std::prev(dp.end()) = m[s.substr(s.size() - 1, 1)];
        *std::prev(dp.end(), 2) = m[s.substr(s.size() - 2, 1)] * dp.back() + m[s.substr(s.size() - 2, 2)];

        constexpr int modulus = 1e9 + 7;

        for (auto i = s.size() - 3; i != -1; --i) {
            dp.at(i) = (m[s.substr(i, 1)] * dp.at(i + 1) + m[s.substr(i, 2)] * dp.at(i + 2)) % modulus;
        }

        return dp.front();
    }
};
