
/*
You are given an array nums consisting of positive integers and an integer k.

Partition the array into two ordered groups such that each element is in
exactly one group. A partition is called great if the sum of elements of each
group is greater than or equal to k.

Return the number of distinct great partitions. Since the answer may be too
large, return it modulo 10^9 + 7.

Two partitions are considered distinct if some element nums[i] is in different
groups in the two partitions.
*/

class Solution {
public:
    int countPartitions(std::vector<int>& nums, int k)
    {
        if (std::cmp_greater(2 * k, std::reduce(nums.begin(), nums.end(), 0LL))) {
            return 0;
        }

        std::vector<long long> dp(k);
        dp.front() = 1;

        constexpr long long modulus = 1e9 + 7;

        for (const auto& num : nums) {
            for (int i = k - num - 1; i > -1; --i) {
                dp.at(num + i) = (dp.at(num + i) + dp.at(i)) % modulus;
            }
        }

        const auto sum = (std::reduce(dp.begin(), dp.end(), 0LL,  [] (auto x, auto y) { return (x + y) % modulus; }) * 2) % modulus;

        long long count = 1;
        for (long long e = 2, n = nums.size(); n > 0; n >>= 1) {
            if (n & 1) {
                count = (count * e) % modulus;
            }
            e = (e * e) % modulus;
        }

        return ((count - sum) % modulus + modulus) % modulus;
    }
};
