
/*
You are given an array prices where prices[i] is the price of a given stock on
the ith day.

Find the maximum profit you can achieve. You may complete at most two
transactions.

Note: You may not engage in multiple transactions simultaneously (i.e., you
must sell the stock before you buy again).
*/

class Solution {
public:
    int maxProfit(std::vector<int>& prices)
    {
        return auxiliary(prices, 0, 4);
    }

private:
    std::unordered_map<int, int> dp;

    int auxiliary(const std::vector<int>& prices, std::size_t day, unsigned char transactions)
    {
        if (day == prices.size() || transactions == 0) {
            return 0;
        }

        auto key = day * 4 + transactions;

        if (dp.contains(key)) {
            return dp.at(key);
        }

        // Do not transact
        int t0 = auxiliary(prices, day + 1, transactions);

        // Transact
        int t1 = std::pow(-1, 1 - transactions % 2) * prices.at(day) + auxiliary(prices, day + 1, transactions - 1);

        return dp[key] = std::max(t0, t1);
    }
};
