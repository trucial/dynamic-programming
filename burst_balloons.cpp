
/*
You are given n balloons, indexed from 0 to n - 1. Each balloon is painted with
a number on it represented by an array nums. You are asked to burst all the
balloons.

If you burst the ith balloon, you will get nums[i - 1] * nums[i] * nums[i + 1]
coins. If i - 1 or i + 1 goes out of bounds of the array, then treat it as if
there is a balloon with a 1 painted on it.

Return the maximum coins you can collect by bursting the balloons wisely.
*/

class Solution {
public:
    int maxCoins(std::vector<int>& nums)
    {
        auto const n = nums.size();
        std::vector<std::vector<int>> dp(n, std::vector<int>(n));

        for (int r = 0; r < n; ++r) {
            for (int l = r; l > -1; --l) {
                for (int i = l; i < r + 1; ++i) {
                    dp.at(l).at(r) = std::max(dp.at(l).at(r),
                                             (l > 0 ? nums.at(l - 1) : 1) * nums.at(i) * (r < n - 1 ? nums.at(r + 1) : 1)
                                           + (i > l ? dp.at(l).at(i - 1) : 0)
                                           + (i < r ? dp.at(i + 1).at(r) : 0));
                }
            }
        }

        return dp.front().back();
    }
};
