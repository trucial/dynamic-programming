
/*
You are given a square board of characters. You can move on the board starting
at the bottom right square marked with the character 'S'.

You need to reach the top left square marked with the character 'E'. The rest
of the squares are labeled either with a numeric character 1, 2, ..., 9 or with
an obstacle 'X'. In one move you can go up, left or up-left (diagonally) only
if there is no obstacle there.

Return a list of two integers: the first integer is the maximum sum of numeric
characters you can collect, and the second is the number of such paths that you
can take to get that maximum sum, taken modulo 10^9 + 7.

In case there is no path, return [0, 0].
*/

class Solution {
public:
    std::vector<int> pathsWithMaxScore(std::vector<std::string>& board)
    {
        board.front().front() = '0';

        const auto m = board.size();
        const auto n = board.front().size();

        std::vector<std::vector<int>> dp(m, std::vector<int>(n));
        std::vector<std::vector<int>> dp1(m, std::vector<int>(n));
        dp1.back().back() = 1;

        for (int i = m - 2; i > -1; --i) {
            if (board.at(i).back() == 'X') {
                break;
            }
            dp.at(i).back() = board.at(i).back() - '0' + dp.at(i + 1).back();
            dp1.at(i).back() = 1;
        }

        for (int j = n - 2; j > -1; --j) {
            if (board.back().at(j) == 'X') {
                break;
            }
            dp.back().at(j) = board.back().at(j) - '0' + dp.back().at(j + 1);
            dp1.back().at(j) = 1;
        }

        constexpr int mod = 1e9 + 7;

        for (int i = m - 2; i > -1; --i) {
            for (int j = n - 2; j > -1; --j) {
                if (board.at(i).at(j) == 'X') {
                    continue;
                }
                auto u = dp1.at(i + 1).at(j) == 0 ? 0 : dp.at(i + 1).at(j);
                auto l = dp1.at(i).at(j + 1) == 0 ? 0 : dp.at(i).at(j + 1);
                auto d = dp1.at(i + 1).at(j + 1) == 0 ? 0 : dp.at(i + 1).at(j + 1);
                auto max = std::max({u, l, d});
                dp.at(i).at(j) = board.at(i).at(j) - '0' + max;
                dp1.at(i).at(j) = ((u == max) * dp1.at(i + 1).at(j) + (l == max) * dp1.at(i).at(j + 1) + (d == max) * dp1.at(i + 1).at(j + 1)) % mod;
            }
        }

        if (dp1.front().front() == 0) { return {0, 0}; }
        return {dp.front().front(), dp1.front().front()};
    }
};
