
/*
We define str = [s, n] as the string str which consists of the string s
concatenated n times.

For example, str == ["abc", 3] =="abcabcabc". We define that string s1 can be
obtained from string s2 if we can remove some characters from s2 such that it
becomes s1.

For example, s1 = "abc" can be obtained from s2 = "abdbec" based on our
definition by removing the bolded underlined characters. You are given two
strings s1 and s2 and two integers n1 and n2. You have the two strings str1 =
[s1, n1] and str2 = [s2, n2].

Return the maximum integer m such that str = [str2, m] can be obtained from
str1.
*/

class Solution {
public:
    int getMaxRepetitions(std::string s1, int n1, std::string s2, int n2)
    {
        if (s2.size() == 1) {
            return std::count(s1.begin(), s1.end(), *s2.c_str()) * n1 / n2;
        }

        int p1 = 0;
        int p2 = 0;
        int first = 0;

        while (p1 < s1.size() * n1) {
            while (s1.at(p1 % s1.size()) != s2.at(p2 % s2.size()) && p1 < s1.size() * n1) {
                ++p1;
            }

            ++p1;
            ++p2;

            if (p2 % s2.size() == 0) {
                if (p2 == s2.size()) {
                    // Position in s1 where the first s2 ends
                    first = p1;
                } else if (p1 % s1.size() == first % s1.size() && first > 0) {
                    // s1 reaches the same position, which means a repeating cycle
                    int repeat = (s1.size() * n1 - p1) / (p1 - first);
                    p1 += repeat * (p1 - first);
                    p2 += repeat * (p2 - s2.size());
                }
            }
        }

        return p2 / s2.length() / n2;
    }
};
