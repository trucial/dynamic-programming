
/*
A path in a binary tree is a sequence of nodes where each pair of adjacent
nodes in the sequence has an edge connecting them. A node can only appear in
the sequence at most once. Note that the path does not need to pass through the
root.

The path sum of a path is the sum of the node's values in the path.

Given the root of a binary tree, return the maximum path sum of any non-empty
path.
*/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

class Solution {
public:
    int maxPathSum(TreeNode* root)
    {
        return dfs(root).second;
    }

private:
    std::pair<int, int> dfs(const TreeNode* node)
    {
        if (node == nullptr) {
            return {0, std::numeric_limits<int>::min()};
        }

        auto [l0, l1] = dfs(node->left);
        auto [r0, r1] = dfs(node->right);

        // Maximum of node added to the left or right subtree
        auto m0 = std::max(node->val + std::max({l0, r0, 0}), 0);

        // Maximum of node added to the left and right subtrees
        auto m1 = std::max(node->val + std::max(l0, 0) + std::max(r0, 0), std::max(l1, r1));

        return {m0, m1};
    }
};
