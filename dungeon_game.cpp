
/*
The demons had captured the princess and imprisoned her in the bottom-right
corner of a dungeon. The dungeon consists of m x n rooms laid out in a 2D grid.
Our valiant knight was initially positioned in the top-left room and must fight
his way through dungeon to rescue the princess.

The knight has an initial health point represented by a positive integer. If at
any point his health point drops to 0 or below, he dies immediately.

Some of the rooms are guarded by demons (represented by negative integers), so
the knight loses health upon entering these rooms; other rooms are either empty
(represented as 0) or contain magic orbs that increase the knight's health
(represented by positive integers).

To reach the princess as quickly as possible, the knight decides to move only
rightward or downward in each step.

Return the knight's minimum initial health so that he can rescue the princess.

Note that any room can contain threats or power-ups, even the first room the
knight enters and the bottom-right room where the princess is imprisoned.
*/

class Solution {
public:
    int calculateMinimumHP(std::vector<std::vector<int>>& dungeon)
    {
        const auto m = dungeon.size();
        const auto n = dungeon.front().size();

        std::vector<std::vector<int>> dp(m, std::vector<int>(n));

        dp.at(m - 1).at(n - 1) = std::max(1, 1 - dungeon.at(m - 1).at(n - 1));

        for (int i = m - 2; i > -1; --i) {
            dp.at(i).at(n - 1) = std::max(1, dp.at(i + 1).at(n - 1) - dungeon.at(i).at(n - 1));
        }

        for (int j = n - 2; j > -1; --j) {
            dp.at(m - 1).at(j) = std::max(1, dp.at(m - 1).at(j + 1) - dungeon.at(m - 1).at(j));
        }

        for (int i = m - 2; i > -1; --i) {
            for (int j = n - 2; j > -1; --j) {
                dp.at(i).at(j) = std::max(1, std::min(dp.at(i + 1).at(j), dp.at(i).at(j + 1)) - dungeon.at(i).at(j));
            }
        }

        return dp.front().front();
    }
};
