
/*
Given an array of strings words (without duplicates), return all the
concatenated words in the given list of words.

A concatenated word is defined as a string that is comprised entirely of at
least two shorter words (not necessarily distinct) in the given array.
*/

class Solution {
public:
    std::vector<string> findAllConcatenatedWordsInADict(std::vector<std::string>& words)
    {
        std::copy(std::make_move_iterator(words.begin()), std::make_move_iterator(words.end()), std::inserter(words_, words_.begin()));

        std::vector<string> concatenated;

        for (const auto& word : words_) {
            if (is_concatenated_(word)) {
                concatenated.emplace_back(word);
            }
        }

        return concatenated;
    }

private:
    std::unordered_set<std::string> words_;

    std::unordered_map<std::string, bool> dp_;

    bool is_concatenated_(const std::string& s)
    {
        if (dp_.contains(s)) {
            return dp_[s];
        }

        for (auto i = 1; i < s.size(); ++i) {
            auto prefix = s.substr(0, i);
            auto suffix = s.substr(i);
            if (words_.find(prefix) != words_.end() && (words_.find(suffix) != words_.end() || is_concatenated_(suffix))) {
                return dp_[s] = true;
            }
        }

        return dp_[s] = false;
    }
};
