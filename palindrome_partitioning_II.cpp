
/*
Given a string s, partition s such that every substring of the partition is a
palindrome.

Return the minimum cuts needed for a palindrome partitioning of s.
*/

class Solution {
public:
    int minCut(std::string s)
    {
        if (s.size() < 2) {
            return 0;
        }

        std::string_view sv {s};

        // Create boolean matrix of whether the element at given indices is palindromic
        std::vector<std::vector<bool>> palindromic(sv.size(), std::vector<bool>(sv.size()));
        for (std::size_t i = 0; i != sv.size(); ++i) {
            for (std::size_t j = 0; j != sv.size(); ++j) {
               palindromic.at(i).at(j) = is_palindrome(sv.substr(i, j + 1));
            }
        }

        std::vector<int> min(sv.size(), std::numeric_limits<int>::max());
        min.front() = 1;

        for (int i{}; i < sv.size(); ++i) {
            if (palindromic.front().at(i)) {
                min.at(i) = 0;
                continue;
            }

            min.at(i) = min.at(i - 1) + 1;

            for (int j = i - 1; j > 0; --j) {
                if (palindromic.at(i - j).at(j)) {
                    min.at(i) = std::min(min.at(i), min.at(i - j - 1) + 1);
                }
            }
        }

        return min.back();
    }

private:
    bool is_palindrome(std::string_view s)
    {
        return std::equal(s.cbegin(), s.cbegin() + s.size() / 2, s.crbegin());
    }
};
