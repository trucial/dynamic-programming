
/*
Given an integer array nums and an integer k, split nums into k non-empty
subarrays such that the largest sum of any subarray is minimized.

Return the minimized largest sum of the split.

A subarray is a contiguous part of the array.
*/

class Solution {
public:
    int splitArray(std::vector<int>& nums, int k)
    {
        auto l = *std::max_element(nums.begin(), nums.end());
        auto r = std::reduce(nums.begin(), nums.end());

        while (l <= r) {
            auto m = l + (r - l) / 2;
            // Greedy
            int count = 1;
            for (int sum = 0; const auto num : nums) {
                if (num + sum <= m) {
                    sum += num;
                } else {
                    sum = num;
                    ++count;
                }

                if (count > k) {
                    break;
                }
            }

            if (count <= k) {
                r = m - 1;
            } else {
                l = m + 1;
            }
        }

        return l;
    }
};
