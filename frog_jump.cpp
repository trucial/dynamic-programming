
/*
A frog is crossing a river. The river is divided into some number of units, and
at each unit, there may or may not exist a stone. The frog can jump on a stone,
but it must not jump into the water.

Given a list of stones positions (in units) in sorted ascending order,
determine if the frog can cross the river by landing on the last stone.
Initially, the frog is on the first stone and assumes the first jump must be 1
unit.

If the frog's last jump was k units, its next jump must be either k - 1, k, or
k + 1 units. The frog can only jump in the forward direction.
*/

class Solution {
public:
    bool canCross(std::vector<int>& stones)
    {
        std::unordered_map<int, std::unordered_set<int>> m;
        m[1].insert(1);

        for (const auto stone : stones) {
            if (m.contains(stone)) {
                for (const auto j : m[stone]) {
                    if (j - 1 > 0) {
                        m[stone + j - 1].insert(j - 1);
                    }
                    m[stone + j].insert(j);
                    m[stone + j + 1].insert(j + 1);
                }
            }
        }

        return m.contains(stones.back());
    }
};
