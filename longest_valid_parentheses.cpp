
/*
Given a string containing just the characters '(' and ')', return the length of
the longest valid (well-formed) parentheses substring.
*/

class Solution {
public:
    int longestValidParentheses(std::string s)
    {
        if (s.size() < 2) {
            return 0;
        }

        std::stack<int> opens;
        for (int i = 0; i < s.size(); ++i) {
            if (s.at(i) == '(') {
                opens.push(i);
                s.at(i) = '0';
            } else {
                if (not opens.empty()) {
                    s.at(opens.top()) = s.at(i) = '1';
                    opens.pop();
                } else {
                    s.at(i) = '0';
                }
            }
        }

        std::vector<int> v(s.begin(), s.end());
        std::transform(v.begin(), v.end(), v.begin(), [] (int x) { return x == 0x30 ? 0 : 1; });
        std::inclusive_scan(v.begin(), v.end(), v.begin(), [] (int x, int y) { return x * y + y; });
        return *std::max_element(v.begin(), v.end());
    }
};
