
/*
You are installing a billboard and want it to have the largest height. The
billboard will have two steel supports, one on each side. Each steel support
must be an equal height.

You are given a collection of rods that can be welded together. For example, if
you have rods of lengths 1, 2, and 3, you can weld them together to make a
support of length 6.

Return the largest possible height of your billboard installation. If you
cannot support the billboard, return 0.
*/

class Solution {
public:
    int tallestBillboard(std::vector<int>& rods)
    {
        const int sum = std::reduce(rods.begin(), rods.end());
        std::vector<int> dp(sum + 1, -1);
        dp.front() = 0;

        for (const int rod : rods) {
            auto dp0 = dp;
            for (int i = 0; i <= sum - rod; ++i) {
                if (dp0.at(i) < 0) {
                    continue;
                }
                dp.at(i + rod) = std::max(dp.at(i + rod), dp0.at(i));
                dp.at(std::abs(i - rod)) = std::max(dp.at(std::abs(i - rod)), dp0.at(i) + std::min(i, rod));
            }
        }

        return dp.front();
    }
};
