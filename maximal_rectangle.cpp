
/*
Given a rows x cols binary matrix filled with 0's and 1's, find the largest
rectangle containing only 1's and return its area.
*/

class Solution {
public:
    int maximalRectangle(std::vector<std::vector<char>>& matrix)
    {
        if (matrix.size() == 0) {
            return 0;
        }

        const int m = matrix.size();
        const int n = matrix.front().size();

        std::vector<int> left(n);
        std::vector<int> right(n, n);
        std::vector<int> height(n);

        int max = 0;

        for (int i = 0; i < m; ++i) {

            int l = 0;
            int r = n;

            for (int j = 0; j < n; ++j) {
                if (matrix.at(i).at(j) == '1') {
                    ++height.at(j);
                } else {
                    height.at(j) = 0;
                }
            }

            for (int j = 0; j < n; ++j) {
                if (matrix.at(i).at(j) == '1') {
                    left.at(j) = std::max(left.at(j), l);
                } else {
                    left.at(j) = 0;
                    l = j + 1;
                }
            }

            for (int j = n - 1; j > -1; --j) {
                if (matrix.at(i).at(j) == '1') {
                    right.at(j) = std::min(right.at(j), r);
                } else {
                    right.at(j) = n;
                    r = j;
                }
            }

            for (int j = 0; j < n; ++j) {
                max = std::max(max, (right.at(j) - left.at(j)) * height.at(j));
            }
        }

        return max;
    }
};
