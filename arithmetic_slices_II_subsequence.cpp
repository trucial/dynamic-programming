
/*
Given an integer array nums, return the number of all the arithmetic
subsequences of nums.

A sequence of numbers is called arithmetic if it consists of at least three
elements and if the difference between any two consecutive elements is the
same.

For example, [1, 3, 5, 7, 9], [7, 7, 7, 7], and [3, -1, -5, -9] are arithmetic
sequences.

For example, [1, 1, 2, 5, 7] is not an arithmetic sequence.

A subsequence of an array is a sequence that can be formed by removing some
elements (possibly none) of the array.

For example, [2,5,10] is a subsequence of [1,2,1,2,4,1,5,10].

The test cases are generated so that the answer fits in 32-bit integer.
*/

class Solution {
public:
    int numberOfArithmeticSlices(std::vector<int>& nums)
    {
        if (nums.size() < 3) {
            return 0;
        }

        int slices = 0;
        std::vector<std::unordered_map<long int, int>> dp(nums.size());

        for (std::size_t i = 1; i < nums.size(); ++i) {
            for (std::size_t j = 0; j < i; ++j) {
                auto d = static_cast<long int>(nums.at(i)) - nums.at(j);
                dp.at(i)[d] += 1 + dp.at(j)[d];
                slices += dp.at(j)[d];
            }
        }

        return slices;
    }
};
