
/*
Given two strings s and t, return the number of distinct subsequences of s
which equals t.

The test cases are generated so that the answer fits on a 32-bit signed
integer.
*/

class Solution {
public:
    int numDistinct(std::string s, std::string t)
    {
        // Remove unmatched first characters
        if (auto i = s.find_first_of(t.front()); i != std::string::npos) {
            s = s.substr(i);
        }

        std::function<int(int, int)> dfs = [&] (int i, int j)
        {
            if (j == t.length()) {
                return 1;
            }

            if (i == s.length()) {
                return 0;
            }

            if (dp.contains({i, j})) {
                return dp.at({i, j});
            }

            if (s.at(i) == t.at(j)) {
                dp.insert({{i, j}, dfs(i + 1, j + 1) + dfs(i + 1, j)});
            } else {
                dp.insert({{i, j}, dfs(i + 1, j)});
            }

            return dp.at({i, j});
        };

        return dfs(0, 0);
    }

private:
    std::map<std::pair<int, int>, int> dp;
};
