
/*
You are an ant tasked with adding n new rooms numbered 0 to n-1 to your colony.
You are given the expansion plan as a 0-indexed integer array of length n,
prevRoom, where prevRoom[i] indicates that you must build room prevRoom[i]
before building room i, and these two rooms must be connected directly. Room 0
is already built, so prevRoom[0] = -1. The expansion plan is given such that
once all the rooms are built, every room will be reachable from room 0.

You can only build one room at a time, and you can travel freely between rooms
you have already built only if they are connected. You can choose to build any
room as long as its previous room is already built.

Return the number of different orders you can build all the rooms in. Since the
answer may be large, return it modulo 10^9 + 7.
*/

class Solution {
public:
    int waysToBuildRooms(std::vector<int>& prevRoom)
    {
        const int n = prevRoom.size();

        inverse_.resize(n + 1);
        for (long long i = 1; i <= n; ++i) {
            ans_ = ans_ * i % mod_;
            inverse_.at(i) = pow_(i, mod_ - 2);
        }

        tree_.resize(n);
        for (int i = 0; i < n; ++i) {
            if (prevRoom.at(i) != -1) {
                tree_.at(prevRoom.at(i)).push_back(i);
            }
        }

        dfs_(0);
        return ans_;
    }

private:
    static constexpr int mod_ = 1e9 + 7;

    int ans_ = 1;

    std::vector<long long> inverse_;

    std::vector<std::vector<int>> tree_;

    int pow_(long long x, int y)
    {
        long long ans = 1;
        for (; y > 0; y >>= 1) {
            if (y & 1) {
                ans = ans * x % mod_;
            }
            x = x * x % mod_;
        }

        return ans;
    }

    int dfs_(int node)
    {
        int count = 1;

        for (const auto i : tree_.at(node)) {
            count += dfs_(i);
        }

        ans_ = ans_ * inverse_.at(count) % mod_;
        return count;
    }
};
