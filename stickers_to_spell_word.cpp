
/*
We are given n different types of stickers. Each sticker has a lowercase
English word on it.

You would like to spell out the given string target by cutting individual
letters from your collection of stickers and rearranging them. You can use each
sticker more than once if you want, and you have infinite quantities of each
sticker.

Return the minimum number of stickers that you need to spell out target. If the
task is impossible, return -1.

Note: In all test cases, all words were chosen randomly from the 1000 most
common US English words, and target was chosen as a concatenation of two random
words.
*/

class Solution {
public:
    int minStickers(std::vector<std::string>& stickers, std::string target)
    {
        counts.reserve(stickers.size());
        for (const auto& sticker : stickers) {
            std::unordered_map<char, int> m;
            for (const auto c : sticker) {
                ++m[c];
            }
            counts.push_back(m);
        }

        auto ans = dfs(target, std::unordered_map<char, int>());
        return ans != std::numeric_limits<int>::max() ? ans : -1;
    }

private:
    std::vector<std::unordered_map<char, int>> counts;

    std::unordered_map<std::string, int> dp;

    int dfs(std::string target, std::unordered_map<char, int> sticker)
    {
        if (dp.contains(target)) {
            return dp[target];
        }

        int res = sticker.empty() ? 0 : 1;
        std::string remainder;

        for (const auto& c : target) {
            if (sticker.contains(c) && sticker[c] > 0) {
                --sticker[c];
            } else {
                remainder += c;
            }
        }

        if (!remainder.empty()) {
            auto used = std::numeric_limits<int>::max();
            for (const auto& m : counts) {
                if (!m.contains(remainder.front())) {
                    continue;
                }
                used = std::min(used, dfs(remainder, m));
            }

            dp[remainder] = used;

            if (used != std::numeric_limits<int>::max()) {
                res += used;
            } else {
                res = used;
            }
        }
        return res;
    }
};
