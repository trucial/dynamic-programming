
/*
Given n non-negative integers representing an elevation map where the width of
each bar is 1, compute how much water it can trap after raining.
*/

class Solution {
public:
    int trap(std::vector<int>& height)
    {
        if (height.size() == 1) {
            return 0;
        }

        std::vector<int> left;
        left.reserve(height.size());
        left.push_back(0);

        for (const auto i : height) {
            left.push_back(std::max(left.back(), i));
        }

        std::vector<int> right = height;
        auto it1 = right.begin();
        auto it2 = std::max_element(it1, right.end());

        do {
            std::fill(it1, std::next(it2), *it2);
            it1 = it2;
            it2 = std::max_element(std::next(it1), right.end());
        } while (it2 != right.end());
        right.push_back(0);

        int rain = 0;

        for (auto i = 0; i < height.size(); ++i) {
            rain += std::max(std::min(left.at(i), right.at(i)) - height.at(i), 0);
        }

        return rain;
    }
};
