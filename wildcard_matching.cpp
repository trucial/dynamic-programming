
/*
Given an input string (s) and a pattern (p), implement wildcard pattern
matching with support for '?' and '*' where:

'?' Matches any single character.
'*' Matches any sequence of characters (including the empty sequence).

The matching should cover the entire input string (not partial).
*/

class Solution {
public:
    bool isMatch(std::string s, std::string p)
    {
        std::vector<std::vector<bool>> dp(s.size() + 1, std::vector(p.size() + 1, false));

        dp.front().front() = true;
        for (auto j = 0; j != p.size() && p.at(j) == '*'; ++j) {
            dp.front().at(j + 1) = true;
        }

        for (auto i = 1; i != s.size() + 1; ++i) {
            for (auto j = 1; j != p.size() + 1; ++j) {
                if ((s.at(i - 1) == p.at(j - 1) || p.at(j - 1) == '?') && dp.at(i - 1).at(j - 1)) {
                    dp.at(i).at(j) = true;
                } else if (p.at(j - 1) == '*') {
                    dp.at(i).at(j) = dp.at(i - 1).at(j) || dp.at(i).at(j - 1);
                }
            }
        }

        return dp.back().back();
    }
};
