/*
You are given several boxes with different colors represented by different
positive numbers.

You may experience several rounds to remove boxes until there is no box left.
Each time you can choose some continuous boxes with the same color (i.e.,
composed of k boxes, k >= 1), remove them and get k * k points.

Return the maximum points you can get.
*/

class Solution {
public:
    int removeBoxes(std::vector<int>& boxes)
    {
        boxes_ = std::move(boxes);
        return solve_(0, boxes_.size() - 1, 0);
    }

private:
    std::vector<int> boxes_;

    int dp_[100][100][100];

    int solve_(int l, int r, int c)
    {
        if (l > r) {
            return 0;
        }

        if (dp_[l][r][c]) {
            return dp_[l][r][c];
        }

        auto ans = std::pow(c + 1, 2) + solve_(l + 1, r, 0);

        for (int m = l + 1; m <= r; ++m) {
            if (boxes_[m] == boxes_[l]) {
                ans = std::max<int>(ans, solve_(l + 1, m - 1, 0) + solve_(m, r, c + 1));
            }
        }

        return dp_[l][r][c] = ans;
    }
};
