
/*
Given a string s and a dictionary of strings wordDict, add spaces in s to
construct a sentence where each word is a valid dictionary word. Return all
such possible sentences in any order.

Note that the same word in the dictionary may be reused multiple times in the
segmentation.
*/

class Solution {
public:
    std::vector<std::string> wordBreak(std::string s, std::vector<std::string>& wordDict)
    {
        std::copy(std::begin(wordDict), std::end(wordDict), std::inserter(dict, std::begin(dict)));

        return dfs(s);
    }

private:
    std::unordered_set<std::string> dict;

    std::vector<std::string> dfs(std::string s)
    {
        std::vector<std::string> result;

        for (int i = 1; i < s.size() + 1; ++i) {
            if (const auto prefix = s.substr(0, i); dict.contains(prefix)) {
                if (prefix == s) {
                    result.push_back(prefix);
                } else {
                    for (const auto suffixes = dfs(s.substr(i)); const auto& suffix : suffixes) {
                        result.push_back(prefix + ' ' + suffix);
                    }
                }
            }
        }

        return result;
    }
};
